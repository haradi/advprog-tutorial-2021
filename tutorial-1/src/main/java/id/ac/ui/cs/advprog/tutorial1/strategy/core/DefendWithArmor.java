package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    //ToDo: Complete me
    private String name;
    public DefendWithArmor(){
        this.name = "Armor";
    }

    @Override
    public String defend(){
        return "Armor defense";
    }

    @Override
    public String getType() {
        return this.name;
    }
}
