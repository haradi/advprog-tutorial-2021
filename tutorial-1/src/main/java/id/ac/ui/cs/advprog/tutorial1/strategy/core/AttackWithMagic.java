package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    //ToDo: Complete me
    private String name;
    public AttackWithMagic(){
        this.name = "Magic";
    }

    @Override
    public String attack(){
        return "Magic spell";
    }

    @Override
    public String getType() {
        return this.name;
    }
}
