package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {

    //ToDo: Complete me

    private String name;
    //private AttackBehavior attackBehavior;
    //private DefenseBehavior defenseBehavior;

    public KnightAdventurer(){
        this.name = "Knight";
        this.setAttackBehavior(new AttackWithSword());
        this.setDefenseBehavior(new DefendWithArmor());
    }

    @Override
    public String getAlias(){
        return this.name;
    }
}
