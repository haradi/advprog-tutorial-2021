package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {

    //ToDo: Complete me

    private String name;
    //private AttackBehavior attackBehavior;
    //private DefenseBehavior defenseBehavior;

    public MysticAdventurer(){
        this.name = "Mystic";
        this.setAttackBehavior(new AttackWithMagic());
        this.setDefenseBehavior(new DefendWithShield());
    }

    @Override
    public String getAlias(){
        return this.name;
    }
}
