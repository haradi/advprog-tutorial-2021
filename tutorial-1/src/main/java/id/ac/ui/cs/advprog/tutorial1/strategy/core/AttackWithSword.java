package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    //ToDo: Complete me
    private String name;
    public AttackWithSword(){
        this.name = "Sword";
    }

    @Override
    public String attack(){
        return "Sword slash";
    }

    @Override
    public String getType() {
        return this.name;
    }
}
