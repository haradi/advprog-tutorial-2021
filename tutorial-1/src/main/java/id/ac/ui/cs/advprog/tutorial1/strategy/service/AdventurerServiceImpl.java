package id.ac.ui.cs.advprog.tutorial1.strategy.service;

import id.ac.ui.cs.advprog.tutorial1.strategy.core.*;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.AdventurerRepository;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;
import org.springframework.stereotype.Service;

@Service
public class AdventurerServiceImpl implements AdventurerService {

    private final AdventurerRepository adventurerRepository;

    private final StrategyRepository strategyRepository;


    public AdventurerServiceImpl(
            AdventurerRepository adventurerRepository,
            StrategyRepository strategyRepository) {

        this.adventurerRepository = adventurerRepository;
        this.strategyRepository = strategyRepository;
    }

    @Override
    public Iterable<Adventurer> findAll() {
        return adventurerRepository.findAll();
    }

    @Override
    public Adventurer findByAlias(String alias) {
        return adventurerRepository.findByAlias(alias);
    }

    @Override
    public void changeStrategy(String alias, String attackType, String defenseType) {
        //ToDo: Complete me
        AttackBehavior tempAttackBehavior = attackType.equals("Gun") ? strategyRepository.getAttackBehaviorByType("Gun")
                : attackType.equals("Sword") ? strategyRepository.getAttackBehaviorByType("Sword")
                : strategyRepository.getAttackBehaviorByType("Magic");
        DefenseBehavior tempDefenseBehavior = defenseType.equals("Armor") ? strategyRepository.getDefenseBehaviorByType("Armor")
                : defenseType.equals("Shield") ? strategyRepository.getDefenseBehaviorByType("Shield")
                : strategyRepository.getDefenseBehaviorByType("Barrier");

        Adventurer tempAdventurer = findByAlias(alias);
        tempAdventurer.setAttackBehavior(tempAttackBehavior);
        tempAdventurer.setDefenseBehavior(tempDefenseBehavior);

    }

    @Override
    public Iterable<AttackBehavior> getAttackBehaviors() {
        return strategyRepository.getAttackBehaviors();
    }

    @Override
    public Iterable<DefenseBehavior> getDefenseBehaviors() {
        return strategyRepository.getDefenseBehaviors();
    }
}
