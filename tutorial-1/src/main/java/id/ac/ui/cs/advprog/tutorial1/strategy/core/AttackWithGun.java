package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    //ToDo: Complete me
    private String name;
    public AttackWithGun(){
        this.name = "Gun";
    }

    @Override
    public String attack(){
        return "Gun shot";
    }

    @Override
    public String getType() {
        return this.name;
    }
}
