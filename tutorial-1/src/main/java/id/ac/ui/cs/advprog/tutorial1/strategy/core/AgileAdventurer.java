package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
	
    //ToDo: Complete me
    private String name;
    //private AttackBehavior attackBehavior;
    //private DefenseBehavior defenseBehavior;

    public AgileAdventurer(){
        this.name = "Agile";
        this.setAttackBehavior(new AttackWithGun());
        this.setDefenseBehavior(new DefendWithBarrier());
    }

    @Override
    public String getAlias(){
        return this.name;
    }
}
